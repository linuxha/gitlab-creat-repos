#!/bin/bash

# @FIXME: Need to add a simple help option -h -? --help
if [ $# -gt 1 ]; then
    echo "Create a new Gitlab repository from the current directory"
    exit 1
fi

# Probably should also do a name check
# Not sure things like spaces will work well
NOM=$1

# This allows the user to override the setting by
# export VARNAME on the CLI
GITLABUSER="${GITLABUSER-$(git config gitlab.user)}"
if [ -z "${GITLABUSER}" ]; then
    echo "GITLab login User name undefined (gitlab.user)"
    exit 1
fi
GITNAME="${GITNAME-(git config user.name)}"
if [ -z "${GITNAME}" ]; then
    echo "User name undefined"
    exit 1
fi
GITEMAIL="${GITEMAIL-$(git config user.email)}"

# Hmm, we should check that it's not already a repo
# just not sure how yet

#
if [ -z "${NOM}" ]; then
    # get the name of the current directory and use that
    NOM=$(basename ${PWD})
else
    if [ ! -d "${NOM}" ]; then
	echo "Repo: ${NOM} does not exist!"
	exit 2
    fi
    cd "${NOM}"
fi

# ------------------------------------------------------------------------------
# This is for private
# what about public?
# ------------------------------------------------------------------------------
# Get from your profile/account (where do we get it, a note might be nice)
# need to protect this
PRIVATE_TOKEN=$(git config gitlab.private-token)

# I'll add a flag later
PUBLIC="${PUBLIC-true}"
if [ "${PUBLIC}" != "false" ]; then
    PUBLIC="true"
fi

ISSUES="${ISSUES-false}"
if [ "${ISSUES}" != "false" ]; then
    PUBLIC="true"
fi


# Uses https
# what about ssh?
STR=$(curl --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -H "Content-Type: application/json" -d "{\"name\":\"${NOM}\",\"issues_enabled\":\"${ISSUES}\",\"public\":\"${PUBLIC}\"}" -X POST "https://gitlab.com/api/v3/projects?name=${NOM}" )
# Not doing anything with it yet
# I should check for failure
echo "${STR}" | tee .git-response

# Should make this an option
mkdir -p t
echo -e '.gitignore\nt/*' >> .gitignore

git init
git remote add origin git@gitlab.com:linuxha/${NOM}.git
git add .
git commit -m "Initial commit"
# @FIXED: $ git push -u origin master
#           error: src refspec master does not match any.
#           error: failed to push some refs to 'git@gitlab.com:linuxha/truckin.git'
# Fix:    Need a valid ssh key
git push -u origin master

