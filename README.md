# gitlab-create-repos.sh READEM

# Description
This scrip will ask you for various input to login and setup a new GIT repos on GITLAB. It will also create a ./t directory (for temporary junk that you don't want sent to the repos) and a .gitignore with the .gitignore and t/* in it.

When run this script expects to be in the git repos root directory. It also expects that you will have some of your GITLAB information in the ~/.gitconfig file.

```
[gitlab]
        user = user_name
        private-token = private_gitlab_token
```

Please remember to keep the private-token private.

This script requires that you have your ssh keys properly setup in git and on Gitlab.

# Todo
* Provide better documentation (this supports setting the repos as public or private on create)
* provide better error handling

# Author
ncherry@linuxha.com

# Copyright
2017

# License
GPLv2
